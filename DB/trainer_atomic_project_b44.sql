-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2017 at 07:31 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainer_atomic_project_b44`
--
CREATE DATABASE IF NOT EXISTS `trainer_atomic_project_b44` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `trainer_atomic_project_b44`;

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Pyramid', 'Tom Martin', 'No'),
(2, 'Inferno', 'Dan Brown', 'No'),
(3, 'Himu', 'Humayun Ahmed', 'Yes'),
(4, 'vcchg', 'fdsfd', 'No'),
(5, 'gdfgd', '547646', 'Yes'),
(6, 'ertdy', '346477', 'No'),
(7, 'fdsd fs', 'sfd sf df', 'No'),
(8, 'Durbin', 'Shirshendu', 'Yes'),
(9, 'sfds', 'sdfertwt3', 'No'),
(10, 'dfs', 'fds sar', 'No'),
(11, 'sdfsf', 'sdfsg', 'No'),
(12, 'Mr Y', 'Mr X', 'No'),
(13, 'fdgdfg', 'dsfgdsdyr', 'No'),
(14, 'stest', 'fdsgdsg', 'No'),
(15, 'sdfsfs', 'sfdsf', 'No'),
(16, 'sdgfs', 'sdgf', 'No'),
(17, 'dfs fd', 'sdf sgt ert', 'No'),
(18, 'dfs fd', 'sdf sgt ert', 'No'),
(19, 'dsfs f', 'dy rt ryry', 'No'),
(20, 'dsfs f', 'dy rt ryry', 'No'),
(21, 'dfsf', 'sdfsf', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
